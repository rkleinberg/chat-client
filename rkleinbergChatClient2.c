#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <sys/select.h>
#include <sys/time.h>
#include <ncurses.h>
#include <curses.h>
#include <pthread.h>
//https://www.tutorialspoint.com/c_standard_library/c_function_strcat.htm - strcat documentation
//http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/client.c - to look at how to make a sockaddr_in
//https://www.geeksforgeeks.org/taking-string-input-space-c-3-different-methods/ - scanf documentation
//https://stackoverflow.com/questions/8107826/proper-way-to-empty-a-c-string - memset function
//https://www.youtube.com/watch?v=pjT5wq11ZSE - ncurses tutorial
//I worked with the following people on my project:
//	Jared Wilkens - ncurses and multi-threading
//	Christian Alvo - ncurses and mulit-threading
//  Matthew Coffman - multi-threading
//  Matthew Miller - ncurses
//
//I have help to:
//  Matthew Beall
//  Drew Astin

//define global variables
#define SERVER "10.115.20.250"
#define PORT 49153
#define BUFSIZE 1024
int fd, len;
WINDOW * big_win;
WINDOW * win;
WINDOW * send_win;
WINDOW * big_send_win;


//Code primarily taken from John Rodkey's presentaion - connects to server
int connect2v4stream(char * srv, int port){
	//define return and sockaddr_in
	int ret, sockd;
	struct in_addr myIn_addr;
	struct sockaddr_in mySockaddr_in;

	if( (sockd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		printf("ERROR: Error creating socket. errno = %d\n", errno);
		exit(errno);
	}
	if( (ret = inet_pton(AF_INET, "10.115.20.250", &myIn_addr)) <= 0){
		printf("ERROR: trouble converting using inet_pton. return value = %d, errno = %d\n", ret, errno);
		exit(errno);
	
	}

	//fill the sockaddr_in with relevant information
	mySockaddr_in.sin_family = AF_INET;
	mySockaddr_in.sin_port = htons(PORT);
	mySockaddr_in.sin_addr = myIn_addr;

	if( (connect(sockd, (struct sockaddr *) &mySockaddr_in, sizeof(mySockaddr_in))) == -1){
		printf("ERROR: trouble cocnnecting to server, errno = %d\n", errno);
		exit(errno);
	}
	return sockd;
}

//Code Primarily taken fro John Rodkey's presentation - send message to server
int sendout(int fd, char *msg){

	int ret;
	if( strlen(msg) > 1){
		//if the message has substance send it - this stops from sending while scrolling
		ret = send(fd, msg, strlen(msg), 0);
	}
	
	if( ret == -1 ){
		printf("ERROR: trouble sending. errno = %d\n", errno);
		exit(errno);
	}

	//display message on sending window
	wrefresh(send_win);
	cbreak();
	
	return strlen(msg);
}

//make the windows and boxes to display received and sending text
void mk_box(){

	initscr();
    wrefresh(win);
	int height, width;

	//get the dimensions of the user's window
	getmaxyx(stdscr, height, width);
	width = width / 2;


	//create and position the windows according the the size of the user's window
	big_win = newwin( height - 5, width - 3, 1, 1 );
	win = newwin( height - 7, width - 5, 2, 2 );
	big_send_win = newwin( height - 5, width - 10, 1, width + 1 );
	send_win = newwin( height - 7, width - 12, 2, width + 2 );
	refresh();

	//enable scrolling on all the windows, and set the cursor to move to the send window
	scrollok(big_win, TRUE);
	scrollok(big_send_win,TRUE);
	keypad(send_win, TRUE);
	wmove(send_win, 1, 0);
	scrollok(send_win, TRUE);
	scrollok(win, TRUE);


	//make sure to refresh all the windows after edits are made
	wrefresh(send_win);
	wrefresh(big_win);
	refresh();

	//create the recieve box so that they can be visible to the user
	wrefresh(win);
	box(big_win, 0, 0);
	wrefresh(big_win);
	refresh();

	//create the send box to be visible to the user
	box(big_send_win, 0, 0);
	wrefresh(send_win);
	wrefresh(big_send_win);
	refresh();

}


//Code primarily taken from John Rodkey's presentation - recieves text from the server and 
// displays it in a window
void recvandprint(int fd, char *buff) {
	int ret;

	
	
	for(;;) {


		buff = malloc(BUFSIZE+1);
		ret = recv(fd, buff, BUFSIZE,0);
		if(ret==-1) {
			if(errno = EAGAIN){
				break; // do it again
			} else {
				printf("ERROR: error receiveing. errno = %d\n", errno);
				exit(errno);
			}
		} else if(ret == 0 ){
			exit(0);
		} else {
			buff[ret] = 0;
			//printf("%s", buff);
			

			cbreak();
			
			//display code in the receive window
			wprintw(win, buff);
			wrefresh(win);


			
		}
		free(buff);
	}

}

//create multi-thread function fro reading from the server
void* readThread(void* param){
	//make infinite calls to recvandprint
	for(;;){
		char* b;
		recvandprint( fd, b);
	}
}
	
//create multi-thread function for sending to the server
void* sendThread(void* param){
	char *buffer, *origbuffer;
	int is_done = 0;	
	while( ! is_done ) {		

		len = BUFSIZE;
		buffer = malloc(len+1);
		origbuffer = buffer;

		//get the message
		wgetstr(send_win, buffer);
		strcat(buffer, "\n");
		wrefresh(send_win);
		
		//send the message
		sendout(fd, buffer);
		

		is_done = (strcmp (buffer, "quit\n") == 0);
		free(origbuffer);
	}
	endwin();
	exit(1);

}

int main(int argc, char * argv[]) {
	//get username from the user
	printf("Welcome to the chat program, type 'quit' to exit\n" );
	printf("Enter desired user name: ");
	char input[100] = "hmm";
	

	fgets(input, 100, stdin);


	printf("Your username is: %s", input);
	printf("\n");


	//define threads and other variables
	pthread_t read_T;
	pthread_t send_T;
	char *name, *buffer, *origbuffer;
	struct timeval timev;


	//connect to the server
	fd = connect2v4stream( SERVER, PORT );

	//Setupt recv timeout for .5 seconds
	timev.tv_sec = 0;
	timev.tv_usec = 1000 * 500;
	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));

	//send the name
	name = input;
	len = strlen(input);
	input[len] = '\0';
	sendout(fd, input);

	//initialize and create windows for display
	initscr();
	mk_box();
	
	//start mulit-threading process
	pthread_create(&read_T, NULL, readThread, NULL);
	pthread_create(&send_T, NULL, sendThread, NULL);

	pthread_join(read_T, NULL);
	pthread_join(send_T, NULL);

	endwin();

}